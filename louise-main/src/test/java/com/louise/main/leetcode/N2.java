package com.louise.main.leetcode;

import org.junit.jupiter.api.Test;

/**
 * N2
 *
 * @author louise
 * @date 2024/3/28
 */
public class N2 {

    @Test
    public void test() {
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {

        return null;
    }




    static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}