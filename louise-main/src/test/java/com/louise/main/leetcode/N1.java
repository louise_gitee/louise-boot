package com.louise.main.leetcode;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;

/**
 * N1
 *
 * @author louise
 * @date 2024/3/28
 */
public class N1 {

    @Test
    public void test() {
        System.out.println(Arrays.toString(twoSum(new int[]{5, 3, 2, 7}, 9)));
        System.out.println(Arrays.toString(twoSum1(new int[]{5, 3, 2, 7}, 9)));
    }

    /**
     * 暴力解
     */
    public int[] twoSum(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    return new int[]{i, j};
                }
            }
        }
        return null;
    }

    public int[] twoSum1(int[] nums, int target) {
        HashMap<Integer, Integer> diffIndexMap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            Integer index = diffIndexMap.get(nums[i]);
            if (index != null) {
                return new int[]{index, i};
            }
            diffIndexMap.put(target - nums[i], i);
        }
        return null;
    }
}