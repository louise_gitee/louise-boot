package com.louise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LouiseApplication {

    public static void main(String[] args) {
        SpringApplication.run(LouiseApplication.class, args);
    }

}
