package com.louise.main.models.mapper;

import com.louise.main.models.entity.ConsumptionRecord;
import com.louise.main.models.entity.vo.ConsumptionRecordReport;
import org.apache.ibatis.annotations.Mapper;
import com.louise.core.config.mybatisplus.common.BasicMapper;

import java.util.List;

/**
 * consumption_record Mapper
 *
 * @author 92017
 * 2024-04-14 23:20:00
 */
@Mapper
public interface ConsumptionRecordMapper extends BasicMapper<ConsumptionRecord> {

    List<ConsumptionRecordReport> reportList();
}




