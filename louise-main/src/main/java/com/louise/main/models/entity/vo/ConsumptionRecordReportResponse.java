package com.louise.main.models.entity.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ConsumptionRecordReportResponse {

    List<String> titleList;

    List<Series> seriesList;

    @Data
    public static class Series {
        String name;

        BigDecimal[] data;

        String type = "bar";

        public Series(String name, BigDecimal[] data) {
            this.name = name;
            this.data = data;
        }
    }

    public ConsumptionRecordReportResponse(List<String> titleList, List<Series> seriesList) {
        this.titleList = titleList;
        this.seriesList = seriesList;
    }
}
