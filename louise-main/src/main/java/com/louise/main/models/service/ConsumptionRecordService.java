package com.louise.main.models.service;

import com.louise.main.models.entity.ConsumptionRecord;
import com.louise.main.models.entity.vo.ConsumptionRecordReportResponse;
import com.louise.main.models.entity.vo.ConsumptionRecordVo;
import com.louise.core.config.mybatisplus.common.BasicService;

/**
 * consumption_record Service
 *
 * @author 92017
 * 2024-04-14 23:20:00
 */
public interface ConsumptionRecordService extends BasicService<ConsumptionRecord, ConsumptionRecordVo> {

    ConsumptionRecordReportResponse report();
}
