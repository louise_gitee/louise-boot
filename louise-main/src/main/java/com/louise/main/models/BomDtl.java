package com.louise.main.models;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * tm_bom_dtl
 *
 * @author lll
 * @date 2022/08/10 16:14
 */
@Data
@TableName("tm_bom_dtl")
public class BomDtl {
    @TableId(type = IdType.ASSIGN_ID)
    private Long tid;

    /**
     * 整车编码
     */
    @TableField("material_code")
    private String materialCode;

    /**
     * 整车编码描述
     */
    @TableField("material_name")
    private String materialName;

    /**
     * 物料ID
     */
    @TableField("product_id")
    private String productId;

    /**
     * 物料编码
     */
    @TableField("product_no")
    private String productNo;

    /**
     * 物料描述
     */
    @TableField("product_name")
    private String productName;

    /**
     * 工位编码
     */
    @TableField("workstation_no")
    private String workstationNo;

    /**
     * 单车用量
     */
    @TableField("material_dosage")
    private Long materialDosage;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    /**
     * 排序BOM主表ID
     */
    @TableField("sort_bom_id")
    private Long sortBomId;

    /**
     * 更新人
     */
    /*@TableField("update_name")
    private String updateName;*/



    /**
     * 0 排序料 1 其他
     */
    @TableField("type")
    private String type;

    /**
     * 物料排序分类编码
     */
    @TableField("sort_material_no")
    private String sortMaterialNo;

    /**
     * 物料排序分类名称
     */
    @TableField("sort_material_name")
    private String sortMaterialName;

    /**
     * 物料排序分类ID
     */
    @TableField("sort_material_id")
    private String sortMaterialId;

    /**
     * 工位名称
     */
    @TableField("workstation_name")
    private String workstationName;

    /**
     * 配送工位表ID
     */
    @TableField("workstation_id")
    private String workstationId;

    /**
     * 排序料架类型信息ID
     */
    @TableField("sort_type_id")
    private String sortTypeId;

    /**
     * 排序料架类型信息名称
     */
    @TableField("sort_type_name")
    private String sortTypeName;

    /**
     * 排序料架类型信息编号
     */
    @TableField("sort_type_no")
    private String sortTypeNo;

    /**
     * 配送批量(标准存储量)
     */
    @TableField("standard_storage")
    private String standardStorage;

    /**
     * 超时天数
     */
    @TableField("time_outs")
    private String timeOuts;

    /**
     * material_id
     */
    @TableField("material_id")
    private String materialId;

    /**
     * 是否有效
     */
    @TableField("is_valid")
    private String isValid;

    /**
     * 整车编码标记
     */
    @TableField("material_code_label")
    private String materialCodeLabel;



    /**
     * 原始未截取工位号
     */
    @TableField("nosub_workstation_no")
    private String nosubWorkstationNo;


    /**
     * sap拉取车型描述详情
     */
    @TableField("note1")
    private String note1;

    /**
     * sap拉取车型描述
     */
    @TableField("note2")
    private String note2;

    /**
     * mes那边的id
     */
    @TableField(exist = false)
    private Integer tiSapMesBominfoId;
}
