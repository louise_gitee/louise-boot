package com.louise.main.models.entity.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ConsumptionRecordReport {

    String dateStr;

    String consumptionType;

    BigDecimal consumptionAmount;
}
