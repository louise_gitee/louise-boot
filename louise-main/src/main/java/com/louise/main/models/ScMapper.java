package com.louise.main.models;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.louise.dbs.config.DataSource;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.cursor.Cursor;

import java.util.List;

/**
 * ScMapper
 *
 * @author louise
 * @date 2024/4/11
 */
@Mapper
public interface ScMapper extends BaseMapper<BomDtl> {

    @DataSource("lesProd")
    Cursor<BomDtl> lesBom();

    @DataSource("impala")
    void saveImpala(@Param("list") List<BomDtl> bomList);

    @DataSource("sc")
    void saveMysql(@Param("list") List<BomDtl> bomList);
}
