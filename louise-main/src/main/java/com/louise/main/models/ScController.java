package com.louise.main.models;

import cn.hutool.core.collection.CollUtil;
import com.louise.dbs.config.DynamicContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 * @author louise
 * @date 2024/4/11
 */
@Slf4j
@RestController
@RequestMapping("sc")
public class ScController {

    @Resource
    ScMapper scMapper;

    @Resource
    SqlSessionFactory sqlSessionFactory;

    @GetMapping("lesToImpala")
    public void lesToImpala() throws IOException {


        DynamicContextHolder.push("lesProd");
        try (SqlSession sqlSession = sqlSessionFactory.openSession();
             Cursor<BomDtl> cursor = sqlSession.getMapper(ScMapper.class).lesBom()) {
            List<BomDtl> list = new ArrayList<>();
            int i = 0;
            for (BomDtl bomDtl : cursor) {
                i++;
                list.add(bomDtl);
                if(list.size() == 10000){
                    saveMysql(list);
                    list.clear();
                    log.info("num : " + i);
                }
            }

            if(CollUtil.isNotEmpty(list)){
                saveMysql(list);
            }
        }
        DynamicContextHolder.poll();
    }


    private void saveMysql(List<BomDtl> bomList) {
        scMapper.saveMysql(bomList);
    }

    private void saveImpala(List<BomDtl> bomList) {
        scMapper.saveImpala(bomList);
    }
}