package com.louise.main.models.controller;

import com.louise.core.response.R;
import com.louise.main.models.entity.ConsumptionRecord;
import com.louise.main.models.entity.vo.ConsumptionRecordVo;
import com.louise.main.models.service.ConsumptionRecordService;
import com.louise.core.config.mybatisplus.common.BasicController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * consumption_record Controller
 *
 * @author 92017
 * 2024-04-14 23:20:00
 */
@RestController
@RequestMapping("consumptionRecord")
public class ConsumptionRecordController extends BasicController<ConsumptionRecordService, ConsumptionRecord, ConsumptionRecordVo> {

    @GetMapping("report")
    public R report() {
        return R.success(service.report());
    }
}




