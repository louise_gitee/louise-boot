package com.louise.main.models.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.louise.main.models.entity.ConsumptionRecord;
import com.louise.main.models.entity.vo.ConsumptionRecordReport;
import com.louise.main.models.entity.vo.ConsumptionRecordReportResponse;
import com.louise.main.models.entity.vo.ConsumptionRecordVo;
import com.louise.main.models.mapper.ConsumptionRecordMapper;
import com.louise.main.models.service.ConsumptionRecordService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.louise.core.config.mybatisplus.common.AbstractServiceImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * consumption_record Service
 *
 * @author 92017
 * 2024-04-14 23:20:00
 */
@Service
public class ConsumptionRecordServiceImpl extends AbstractServiceImpl<ConsumptionRecordMapper, ConsumptionRecord, ConsumptionRecordVo>
        implements ConsumptionRecordService {

    @Override
    public Wrapper<ConsumptionRecord> getWrapper(ConsumptionRecordVo vo) {
        return new LambdaQueryWrapper<>();
    }

    @Override
    public ConsumptionRecordReportResponse report() {

        List<ConsumptionRecordReport> reportList = baseMapper.reportList();

        List<String> titleList = reportList.stream().map(ConsumptionRecordReport::getDateStr).distinct().collect(Collectors.toList());

        List<ConsumptionRecordReportResponse.Series> seriesList = new ArrayList<>();

        Map<String, List<ConsumptionRecordReport>> reportMap = reportList.stream().collect(Collectors.groupingBy(ConsumptionRecordReport::getConsumptionType));

        for (Map.Entry<String, List<ConsumptionRecordReport>> entry : reportMap.entrySet()) {
            List<ConsumptionRecordReport> value = entry.getValue();
            BigDecimal[] data = new BigDecimal[titleList.size()];
            Arrays.fill(data, BigDecimal.ZERO);

            for (ConsumptionRecordReport consumptionRecordReport : value) {
                int index = titleList.indexOf(consumptionRecordReport.getDateStr());
                data[index] = consumptionRecordReport.getConsumptionAmount();
            }

            seriesList.add(new ConsumptionRecordReportResponse.Series(entry.getKey(), data));
        }

        BigDecimal[] data = new BigDecimal[titleList.size()];
        Arrays.fill(data, BigDecimal.ZERO);
        for (ConsumptionRecordReportResponse.Series series : seriesList) {
            for (int i = 0; i < series.getData().length; i++) {
                data[i] = data[i].add(series.getData()[i]);
            }
        }
        seriesList.add(new ConsumptionRecordReportResponse.Series("合计", data));
        return new ConsumptionRecordReportResponse(titleList, seriesList);
    }
}




