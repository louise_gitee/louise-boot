package com.louise.main.models.entity;

import java.io.Serializable;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * consumption_record
 *
 * @author 92017
 * 2024-04-14 23:20:00
 */
@TableName("consumption_record")
@Data
public class ConsumptionRecord implements Serializable {

    /**
     * 主键
     */
    @TableId
    private Long id;

    /**
     * 消费类型
     */
    private String consumptionType;

    /**
     * 消费便签
     */
    private String consumptionNotes;

    /**
     * 消费金额
     */
    private BigDecimal consumptionAmount;

    /**
     * 消费时间
     */
    private Date consumptionDate;

    /**
     * 创建时间
     */
    private Date crtTime;

    /**
     * 创建人
     */
    private Long crtBy;

    /**
     * 修改时间
     */
    private Date updTime;

    /**
     * 修改人
     */
    private Long updBy;

}
