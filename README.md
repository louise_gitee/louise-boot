# 项目架构
system 用来存放 用户 角色 菜单之类的


# validate√
# 异常处理√
# druid监控√
# 动态数据源√
- 使用数据库存储其他数据源√
# 代码生成器√
- 使用mybatisX生成√
# 公共controller  service  mapper√
- 增删改查导入导出√
- 高级查询
# security
- 白名单 黑名单动态编辑
# 日志框架
- 使用logback 并以天、日志级别进行存储
# 定时任务(quartz)
- 日志没有添加 暂时没找到好方法
# redis集成与监控
# spring监控
# 中间件kafka
# 文件存储
# 数据字典
# 环境变量
# 数据权限管理
# 接口日志
# 配置文件加密


# 用户相关
- 账号锁定
- ip黑名单
- 登录次数
- 角色 权限(菜单)


# 现存问题
- 日志框架 有的sql打不出来
- druid 莫名超时 暂时使用 url后面添加 socketTimeout 和 connectTimeout