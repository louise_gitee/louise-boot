package com.louise.dbs.config.sql;

public class CreateTableSql implements AbstractSql {
    @Override
    public String mysql() {
        return "CREATE TABLE IF NOT EXISTS sys_database_info  (\n" +
                "  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',\n" +
                "  `db_name` varchar(100) NOT NULL COMMENT '数据库名称',\n" +
                "  `driver_class_name` varchar(100) NOT NULL COMMENT '驱动类',\n" +
                "  `url` varchar(100) NOT NULL COMMENT '数据库链接',\n" +
                "  `username` varchar(100) NOT NULL COMMENT '用户名',\n" +
                "  `password` varchar(100) NOT NULL COMMENT '密码',\n" +
                "  `remarks` varchar(100) COMMENT '备注',\n" +
                "  `crt_time` datetime NULL COMMENT '创建时间',\n" +
                "  PRIMARY KEY (`id`),\n" +
                "  UNIQUE KEY `db_name_unique` (`db_name`) USING BTREE\n" +
                "  );";
    }

    @Override
    public String oracle() {
        return "";
    }
}
