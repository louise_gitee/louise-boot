package com.louise.dbs.config.sql;

public class InfoListSql implements AbstractSql {
    @Override
    public String mysql() {
        return "select id,db_name,driver_class_name,url,username,password from sys_database_info";
    }

    @Override
    public String oracle() {
        return null;
    }
}
