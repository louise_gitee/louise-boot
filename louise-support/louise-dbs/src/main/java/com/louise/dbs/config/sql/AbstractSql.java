package com.louise.dbs.config.sql;

public interface AbstractSql {

    default String getSql(String url){
        if(url.contains("mysql")){
            return mysql();
        }
        if(url.contains("oracle")){
            return oracle();
        }
        return mysql();
    }

    String mysql();

    String oracle();
}
