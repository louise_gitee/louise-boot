package com.louise.dbs.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.louise.dbs.config.sql.DbUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;

/**
 * 动态数据源配置
 *
 * @author Louise
 * @date 2022/09/05
 */
@Slf4j
@Configuration
public class DynamicDataSourceConfig {

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.druid")
    public DataSourceProperties dataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    public DataSource dynamicDataSource(DataSourceProperties dataSourceProperties) {
        //默认数据源
        DruidDataSource defaultDataSource = DataSourceProperties.buildDruidDataSource(dataSourceProperties);

        //创建数据库表
        DbUtil.createTable(dataSourceProperties);

        //查询动态数据源
        List<DataSourceProperties> propertiesList = DbUtil.infoList(dataSourceProperties);

        //初始化动态数据源
        HashMap<String, DruidDataSource> targetDataSources = new HashMap<>();
        propertiesList.forEach(properties -> targetDataSources.put(properties.getDbName(),DataSourceProperties.buildDruidDataSource(properties)));

        return new DynamicDataSource(defaultDataSource,targetDataSources);
    }

}