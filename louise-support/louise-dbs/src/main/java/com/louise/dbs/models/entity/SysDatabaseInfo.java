package com.louise.dbs.models.entity;

import lombok.Data;

import java.util.Date;

@Data
public class SysDatabaseInfo {
    Long id;
    /**
     * 库名
     */
    String dbName;
    /**
     * 驱动类
     */
    String driverClassName;
    /**
     * 链接
     */
    String url;
    /**
     * 用户名
     */
    String username;
    /**
     * 密码
     */
    String password;
    /**
     * 备注
     */
    String remarks;
    /**
     * 创建时间
     */
    Date crtTime;

}
