package com.louise.dbs.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.jdbc.datasource.AbstractDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

/**
 * 动态数据来源
 *
 * @author Louise
 * @date 2022/09/05
 */
public class DynamicDataSource extends AbstractDataSource {

    DruidDataSource defaultDataSource;

    Map<String, DruidDataSource> dataSourceMap;

    public DynamicDataSource(DruidDataSource defaultDataSource, Map<String, DruidDataSource> dataSourceMap) {
        this.defaultDataSource = defaultDataSource;
        this.dataSourceMap = dataSourceMap;
    }

    protected DruidDataSource determineTargetDataSource() {
        DruidDataSource target = dataSourceMap.get(DynamicContextHolder.peek());
        return target != null ? target : defaultDataSource;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return determineTargetDataSource().getConnection();
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return determineTargetDataSource().getConnection(username, password);
    }

    @SuppressWarnings("unchecked")
    public <T> T unwrap(Class<T> ic)  {
        return ic.isInstance(this) ? (T) this : this.determineTargetDataSource().unwrap(ic);
    }

    public boolean isWrapperFor(Class<?> ic)  {
        return ic.isInstance(this) || this.determineTargetDataSource().isWrapperFor(ic);
    }
}
