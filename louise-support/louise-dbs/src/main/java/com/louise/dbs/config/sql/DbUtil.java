package com.louise.dbs.config.sql;

import com.louise.core.exception.ServiceException;
import com.louise.dbs.config.DataSourceProperties;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DbUtil {

    public static void createTable(DataSourceProperties properties) {
        try {
            Class.forName(properties.getDriverClassName());
            Connection connection = DriverManager.getConnection(properties.getUrl(), properties.getUsername(), properties.getPassword());

            PreparedStatement preparedStatement = connection.prepareStatement(new CreateTableSql().getSql(properties.getUrl()));
            preparedStatement.executeUpdate();

        } catch (Exception e) {
            throw new ServiceException(500, "创建数据库失败！");
        }
    }

    public static List<DataSourceProperties> infoList(DataSourceProperties properties) {
        List<DataSourceProperties> propertiesList = new ArrayList<>();
        try {
            Class.forName(properties.getDriverClassName());
            Connection connection = DriverManager.getConnection(properties.getUrl(), properties.getUsername(), properties.getPassword());

            PreparedStatement preparedStatement = connection.prepareStatement(new InfoListSql().getSql(properties.getUrl()));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String dbName = resultSet.getString("db_name");
                String driverClassName = resultSet.getString("driver_class_name");
                String url = resultSet.getString("url");
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");

                propertiesList.add(new DataSourceProperties(dbName, driverClassName, url, username, password));
            }
        } catch (Exception e) {
            throw new ServiceException(500, "创建数据库失败！");
        }
        return propertiesList;
    }
}
