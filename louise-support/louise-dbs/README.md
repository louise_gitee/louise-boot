存储在数据库的动态数据源

yml配置
```
spring:
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    druid:
      driver-class-name: com.mysql.jdbc.Driver
      url: jdbc:mysql://127.0.0.1:3306/louise?useSSL=false
      username: root
      password: 123456
```