package com.louise.quartz.models.service;


import com.louise.core.config.mybatisplus.common.BasicService;
import com.louise.quartz.models.entity.SysJob;
import com.louise.quartz.models.entity.vo.SysJobVo;

/**
 * SysJobService
 *
 * @author louise
 * @date 2022/9/20
 */
public interface SysJobService extends BasicService<SysJob, SysJobVo> {
}
