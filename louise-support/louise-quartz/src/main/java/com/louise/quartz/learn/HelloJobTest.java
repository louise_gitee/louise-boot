package com.louise.quartz.learn;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import org.junit.jupiter.api.Test;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * HelloJobTest
 *
 * @author louise
 * @date 2022/9/14
 */
@Component
public class HelloJobTest {

    static {
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        List<Logger> loggerList = loggerContext.getLoggerList();
        loggerList.forEach(logger -> logger.setLevel(Level.INFO));
    }

    @Test
    public void test() throws SchedulerException, InterruptedException {
        //创建调度器
        Scheduler scheduler = getScheduler();
        //创建jobDetail
        JobDetail jobDetail = getJobDetail();
        //创建触发器
        Trigger trigger = getTrigger();
        //将任务和触发器关联并加入调度器进行调度
        scheduler.scheduleJob(jobDetail, trigger);
        //启动调度器
        scheduler.start();

        Thread.sleep(5000);
        //暂停job
        scheduler.pauseJob(jobDetail.getKey());

        Thread.sleep(2000);
        //运行一次
        scheduler.triggerJob(jobDetail.getKey());

        Thread.sleep(5000);
        //恢复job
        scheduler.resumeJob(jobDetail.getKey());

        Thread.sleep(5000);
        //删除job
        scheduler.deleteJob(jobDetail.getKey());

        CountDownLatch countDownLatch = new CountDownLatch(1);
        countDownLatch.await();
    }


    public Scheduler getScheduler() throws SchedulerException {
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.getContext().put("schedulerKey", "schedulerValue");
        return scheduler;
    }

    public JobDetail getJobDetail() {
        return JobBuilder.newJob(HelloJob.class)
                //名称和分组
                .withIdentity("jobName", "jobGroup")
                //参数 为KV形式
                .usingJobData("jobKey", "jobValue")
                //简介
                .withDescription("jobDescription")
                .build();
    }

    public Trigger getTrigger() {
        return TriggerBuilder.newTrigger()
                //名称和分组
                .withIdentity("triggerName", "triggerGroup")
                //立即执行
                //.startNow()
                //开始时间
                //.startAt(new Date())
                //结束时间
                //.endAt()
                //参数 为KV形式
                .usingJobData("triggerKey", "triggerValue")
                //设定优先级 越大越优先执行
                .withPriority(5)
/*                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        //每隔3s执行一次
                        .withIntervalInSeconds(3)
                        //一直执行,如果不写,定时任务就执行一次
                        .repeatForever())*/
                //用七子表达式执行
                .withSchedule(CronScheduleBuilder.cronSchedule("* * * * * ?"))
                //简介
                .withDescription("triggerDescription")
                .build();
    }
}