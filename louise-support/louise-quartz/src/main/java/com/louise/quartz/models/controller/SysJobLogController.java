package com.louise.quartz.models.controller;

import com.louise.quartz.models.entity.SysJobLog;
import com.louise.quartz.models.entity.vo.SysJobLogVo;
import com.louise.quartz.models.service.SysJobLogService;
import com.louise.core.config.mybatisplus.common.BasicController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * sys_job_log Controller
 *
 * @author 92017
 * 2024-04-06 01:02:07
 */
@RestController
@RequestMapping("sysJobLog")
public class SysJobLogController extends BasicController<SysJobLogService, SysJobLog, SysJobLogVo> {

}




