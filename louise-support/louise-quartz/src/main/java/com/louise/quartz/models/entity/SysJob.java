package com.louise.quartz.models.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.louise.core.config.validate.Save;
import com.louise.core.config.validate.Update;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * SysJob
 *
 * @author louise
 * @date 2022/9/19
 */
@Data
public class SysJob implements Serializable {
    /**
     * id
     */
    @TableId
    @NotNull(groups = Update.class, message = "jobId不能为空")
    private Long id;

    /**
     * 任务名称
     */
    @NotEmpty(groups = {Save.class, Update.class}, message = "job名称不能为空")
    private String name;

    /**
     * 容器名称
     */
    @NotEmpty(groups = {Save.class, Update.class}, message = "执行表达式不能为空")
    private String executeExpression;

    /**
     * cron表达式
     */
    @NotEmpty(groups = {Save.class, Update.class}, message = "cron表达式不能为空")
    private String cronExpression;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否开启
     */
    @NotEmpty(groups = {Save.class, Update.class}, message = "是否开启不能为空")
    private Boolean turnOn;

    /**
     * 创建时间
     */
    private Date crtTime;

    /**
     * 修改时间
     */
    private Date updTime;

    /**
     * 创建人
     */
    private Long crtUser;

    /**
     * 修改人
     */
    private Long updUser;

    public SysJob(Long id) {
        this.id = id;
    }

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}