package com.louise.quartz.models.service;

import com.louise.quartz.models.entity.SysJobLog;
import com.louise.quartz.models.entity.vo.SysJobLogVo;
import com.louise.core.config.mybatisplus.common.BasicService;

/**
 * sys_job_log Service
 *
 * @author 92017
 * 2024-04-06 01:02:07
 */
public interface SysJobLogService extends BasicService<SysJobLog, SysJobLogVo> {

    void saveLog(Long jobId, Integer jobResult, String logText);
}
