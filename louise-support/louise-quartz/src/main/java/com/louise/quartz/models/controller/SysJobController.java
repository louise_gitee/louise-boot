package com.louise.quartz.models.controller;

import com.louise.core.config.mybatisplus.common.BasicController;
import com.louise.core.config.validate.Save;
import com.louise.core.config.validate.Update;
import com.louise.core.response.R;
import com.louise.quartz.config.SchedulerJobUtil;
import com.louise.quartz.models.entity.SysJob;
import com.louise.quartz.models.entity.vo.SysJobVo;
import com.louise.quartz.models.service.SysJobService;
import org.quartz.Scheduler;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * SysJobController
 *
 * @author louise
 * @date 2022/9/20
 */
@RestController
@RequestMapping("sysJob")
public class SysJobController extends BasicController<SysJobService, SysJob, SysJobVo> {
    @Resource
    Scheduler scheduler;

    @Override
    public R save(@Validated(Save.class) @RequestBody SysJob sysJob) {
        R rest = super.save(sysJob);
        SchedulerJobUtil.init(scheduler, sysJob);
        return rest;
    }

    @Override
    public R update(@Validated(Update.class) @RequestBody SysJob sysJob) {
        R rest = super.update(sysJob);
        SchedulerJobUtil.reschedule(scheduler, sysJob);
        return rest;
    }

    @Override
    public R delete(@RequestBody Long[] idList) {
        R rest = super.delete(idList);
        for (Long jobId : idList) {
            SchedulerJobUtil.delete(scheduler, new SysJob(jobId));
        }
        return rest;
    }

    @PostMapping("pause")
    public R pause(@RequestBody Long[] idList) {
        for (Long jobId : idList) {
            SchedulerJobUtil.pause(scheduler, new SysJob(jobId));
        }
        return R.success();
    }

    @PostMapping("run")
    public R run(@RequestBody Long[] idList) {
        for (Long jobId : idList) {
            SchedulerJobUtil.run(scheduler, new SysJob(jobId));
        }
        return R.success();
    }
}