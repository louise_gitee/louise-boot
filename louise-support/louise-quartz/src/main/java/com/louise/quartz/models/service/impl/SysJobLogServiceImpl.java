package com.louise.quartz.models.service.impl;

import com.louise.quartz.models.entity.SysJobLog;
import com.louise.quartz.models.entity.vo.SysJobLogVo;
import com.louise.quartz.models.mapper.SysJobLogMapper;
import com.louise.quartz.models.service.SysJobLogService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.louise.core.config.mybatisplus.common.AbstractServiceImpl;

import java.util.Date;

/**
 * sys_job_log Service
 *
 * @author 92017
 * 2024-04-06 01:02:07
 */
@Service
public class SysJobLogServiceImpl extends AbstractServiceImpl<SysJobLogMapper, SysJobLog, SysJobLogVo>
        implements SysJobLogService {

    @Override
    public Wrapper<SysJobLog> getWrapper(SysJobLogVo vo) {
        return null;
    }

    @Override
    @Async
    public void saveLog(Long jobId, Integer jobResult, String logText) {
        save(new SysJobLog(jobId, jobResult, logText, new Date()));
    }
}




