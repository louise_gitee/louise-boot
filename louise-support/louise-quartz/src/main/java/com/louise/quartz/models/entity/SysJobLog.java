package com.louise.quartz.models.entity;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * sys_job_log
 *
 * @author 92017
 * 2024-04-06 01:02:07
 */
@TableName("sys_job_log")
@Data
public class SysJobLog implements Serializable {

    /**
     * 主键
     */
    @TableId
    private Long id;

    /**
     * 定时任务id
     */
    private Long jobId;

    /**
     * 执行结果 0 失败 1 成功
     */
    private Integer jobResult;

    /**
     * 日志内容
     */
    private String logText;

    /**
     * 创建时间
     */
    private Date crtTime;


    public SysJobLog(Long jobId, Integer jobResult, String logText, Date crtTime) {
        this.jobId = jobId;
        this.jobResult = jobResult;
        this.logText = logText;
        this.crtTime = crtTime;
    }
}
