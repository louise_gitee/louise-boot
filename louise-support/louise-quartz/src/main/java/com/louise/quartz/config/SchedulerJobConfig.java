package com.louise.quartz.config;

import com.louise.quartz.models.entity.SysJob;
import com.louise.quartz.models.service.SysJobService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.List;

/**
 * JobConfig
 *
 * @author louise
 * @date 2022/9/19
 */
@Slf4j
@Configuration
public class SchedulerJobConfig {

    public static final String JOB_PREFIX = "JOB_";
    public static final String TRIGGER_PREFIX = "TRIGGER_";
    public static final String JOB_ENTITY = "JOB_ENTITY";

    @Resource
    Scheduler scheduler;

    @Resource
    SysJobService sysJobService;


    /**
     * rescheduleJob 重启
     * pauseJob 暂停
     * triggerJob 运行一次
     * resumeJob 恢复
     * deleteJob 删除
     */
    @Autowired
    public void initJob() {
        List<SysJob> jobList = sysJobService.list();

        for (SysJob sysJob : jobList) {
            try {
                SchedulerJobUtil.init(scheduler, sysJob);
                log.info("{} init success | turn on : {}", sysJob.getName(), sysJob.getTurnOn());
            } catch (Exception e) {
                log.error("{} init failed", sysJob.getName());
            }
        }
    }

    /**
     * @ Autowired
     * 项目结束后清除(暂不使用)
     */
    public void addShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                scheduler.clear();
                log.info("scheduler clear");
            } catch (SchedulerException e) {
                e.printStackTrace();
            }
        }));
    }
}