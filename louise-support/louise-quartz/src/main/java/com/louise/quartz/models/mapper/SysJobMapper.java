package com.louise.quartz.models.mapper;

import com.louise.core.config.mybatisplus.common.BasicMapper;
import com.louise.quartz.models.entity.SysJob;
import org.apache.ibatis.annotations.Mapper;

/**
 * SysJobMapper
 *
 * @author louise
 * @date 2022/9/20
 */
@Mapper
public interface SysJobMapper extends BasicMapper<SysJob> {
}
