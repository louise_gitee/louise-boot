package com.louise.quartz.models.jobs;

import com.louise.core.exception.ServiceException;
import com.louise.quartz.models.entity.SysJob;
import com.louise.quartz.models.service.SysJobService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * SysUserJob
 *
 * @author louise
 * @date 2022/9/20
 */
@Slf4j
@Component("testJob")
public class TestJob implements Serializable {

    @Resource
    SysJobService sysJobService;

    @Test
    public void test() {
        run("111");
        staticMethod(new Date());
        entityMethod();
    }

    public void run(String param) {
        log.info("param : {}", param);
        List<SysJob> list = sysJobService.list();
        throw new ServiceException(500, "测试");
    }

    public static void staticMethod(Date date) {
        System.out.println(new SimpleDateFormat("yyyy-MM-dd").format(date));
    }

    public void entityMethod() {
        System.out.println("entityMethod");
    }
}