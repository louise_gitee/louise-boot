# 一、引入依赖

```
  <dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-quartz</artifactId>
  </dependency>
```

# 二、整体流程

1. 创建自定义 Job 实现 `org.quartz.Job` 接口，这是任务主体，用来写业务逻辑的地方。
2. 创建任务调度器 Scheduler，这是用来调度任务的，主要用于启动、停止、暂停、恢复等操作。
3. 创建任务明细 JobDetail，用于与之前创建的 Job 关联起来
4. 创建触发器 Trigger，触发器是来定义任务的规则的，例如几点执行、几点结束、几分钟执行一次等。这里触发器主要有两大类(SimpleTrigger和CronTrigger)。
5. 根据 Scheduler来 启动 JobDetail 与 Trigger

# 三、创建Job 

```
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
//避免并发执行
@DisallowConcurrentExecution
public class HelloJob implements Job {

    @Override
    public void execute(JobExecutionContext context) {
        Object triggerValue = context.getTrigger().getJobDataMap().get("triggerKey");
        Object jobValue = context.getJobDetail().getJobDataMap().get("jobKey");

        log.info("triggerKey : {}", triggerValue);
        log.info("jobKey : {}", jobValue);

        Object schedulerValue = null;
        try {
            schedulerValue = context.getScheduler().getContext().get("schedulerKey");
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

        log.info("schedulerKey : {}", schedulerValue);
        System.out.println("time : " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")));
    }
}
```

# 四、创建任务调度器Scheduler

main

```
    public Scheduler getScheduler() throws SchedulerException {
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        //可以设置一些KV
        scheduler.getContext().put("schedulerKey", "schedulerValue");
        return scheduler;
    }
```
spring
```
@Autowired
private Scheduler scheduler;
```
# 五、创建任务明细JobDetail

```
public JobDetail getJobDetail() {
    return JobBuilder.newJob(HelloJob.class)
            //名称和分组
            .withIdentity("jobName", "jobGroup")
            //参数 为KV形式
            .usingJobData("jobKey", "jobValue")
            //简介
            .withDescription("jobDescription")
            .build();
}
```

# 六、创建触发器Trigger

```
public Trigger getTrigger() {
    return TriggerBuilder.newTrigger()
            //名称和分组
            .withIdentity("triggerName", "triggerGroup")
            //立即执行
            //.startNow()
            //开始时间
            //.startAt(new Date())
            //结束时间
            //.endAt()
            //参数 为KV形式
            .usingJobData("triggerKey", "triggerValue")
            //设定优先级 越大越优先执行
            .withPriority(5)
/*                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                    //每隔3s执行一次
                    .withIntervalInSeconds(3)
                    //一直执行,如果不写,定时任务就执行一次
                    .repeatForever())*/
            //用七子表达式执行
            .withSchedule(CronScheduleBuilder.cronSchedule("* * * * * ?"))
            //简介
            .withDescription("triggerDescription")
            .build();
}
```

# 七、启动任务

```
@Test
public void test() throws SchedulerException, InterruptedException {
    Scheduler scheduler = getScheduler();
    scheduler.scheduleJob(getJobDetail(), getTrigger());
    scheduler.start();

    CountDownLatch countDownLatch = new CountDownLatch(1);
    countDownLatch.await();
}
```

# 八、任务的暂停、恢复、删除

```
//暂停
scheduler.pauseJob(new JobKey("jobName", "jobGroup"));
//恢复
scheduler.resumeJob(new JobKey("jobName", "jobGroup"));
//删除
scheduler.deleteJob(new JobKey("jobName", "jobGroup"));
//运行一次
scheduler.triggerJob(new JobKey("jobName", "jobGroup"));
```

# 进程锁

使用 `qrtz_locks` 表来进行行锁