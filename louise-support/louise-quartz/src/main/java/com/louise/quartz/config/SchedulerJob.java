package com.louise.quartz.config;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.encoder.Encoder;
import com.louise.core.utils.SpringUtil;
import com.louise.quartz.models.entity.SysJob;
import com.louise.quartz.models.entity.SysJobLog;
import com.louise.quartz.models.service.SysJobLogService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;


/**
 * CommonJob
 *
 * @author louise
 * @date 2022/9/20
 */
@Slf4j
public class SchedulerJob implements Job {

    public static final ThreadLocal<StringBuilder> LOG_LOCAL = new ThreadLocal<>();

    /**
     * "@testJob.run('param1')"                                                           容器内Bean方法
     * "T(louise.common.config.job.jobs.SysUserJob).staticMethod(new java.util.Date())"   运行静态方法
     * "new louise.common.config.job.jobs.SysUserJob().entityMethod()"                    实体类方法
     *
     * @param context 上下文
     */
    @Override
    public void execute(JobExecutionContext context) {
        LOG_LOCAL.set(new StringBuilder());

        JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();

        SysJob jobEntity = (SysJob) jobDataMap.get(SchedulerJobConfig.JOB_ENTITY);

        log.info("executeExpression : {}", jobEntity.getExecuteExpression());

        //运行表达式
        try {
            ExpressionParser parser = new SpelExpressionParser();

            StandardEvaluationContext ctx = new StandardEvaluationContext();

            ctx.setBeanResolver(new BeanFactoryResolver(SpringUtil.getBeanFactory()));

            Expression expression = parser.parseExpression(jobEntity.getExecuteExpression());

            expression.getValue(ctx);

            SpringUtil.getBean(SysJobLogService.class).saveLog(jobEntity.getId(), 0, LOG_LOCAL.get().toString());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            SpringUtil.getBean(SysJobLogService.class).saveLog(jobEntity.getId(), 0, LOG_LOCAL.get().toString());
        } finally {
            LOG_LOCAL.remove();
        }
    }

    public static void appendLog(Encoder<ILoggingEvent> encoder, ILoggingEvent event) {
        StringBuilder builder = LOG_LOCAL.get();
        if (builder == null) {
            return;
        }
        builder.append(new String(encoder.encode(event)));
    }
}