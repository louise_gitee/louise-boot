package com.louise.quartz.config;

import com.louise.quartz.models.entity.SysJob;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;

/**
 * SchedulerJobUtil
 *
 * @author louise
 * @date 2022/9/21
 */
@Slf4j
public class SchedulerJobUtil {

    /**
     * rescheduleJob 重启
     * pauseJob 暂停
     * triggerJob 运行一次
     * resumeJob 恢复
     * deleteJob 删除
     */

    public static void init(Scheduler scheduler, SysJob sysJob) {
        Trigger trigger;
        try {
            trigger = scheduler.getTrigger(getTriggerKey(sysJob.getId()));
        } catch (SchedulerException e) {
            log.error("{} getTrigger failed", sysJob.getName());
            return;
        }
        if (trigger == null) {
            schedule(scheduler, sysJob);
        } else {
            reschedule(scheduler, sysJob);
        }
    }

    /**
     * 定时工作
     *
     * @param scheduler 调度器
     * @param sysJob    系统工作
     */
    public static void schedule(Scheduler scheduler, SysJob sysJob) {
        try {
            scheduler.scheduleJob(getJobDetail(sysJob), getCronTrigger(sysJob));
        } catch (SchedulerException e) {
            log.error("{} schedule failed", sysJob.getName());
        }
        //查看是否开启
        if (!sysJob.getTurnOn()) {
            pause(scheduler, sysJob);
        }
    }

    /**
     * 重启定时工作
     *
     * @param scheduler 调度器
     * @param sysJob    系统工作
     */
    public static void reschedule(Scheduler scheduler, SysJob sysJob) {
        delete(scheduler, sysJob);
        schedule(scheduler, sysJob);
    }

    /**
     * 暂停
     *
     * @param scheduler 调度器
     * @param sysJob    系统工作
     */
    public static void pause(Scheduler scheduler, SysJob sysJob) {
        try {
            scheduler.pauseJob(getJobKey(sysJob.getId()));
        } catch (SchedulerException e) {
            log.error("{} pause failed", sysJob.getName());
        }
    }

    public static void delete(Scheduler scheduler, SysJob sysJob) {
        try {
            scheduler.deleteJob(getJobKey(sysJob.getId()));
        } catch (SchedulerException e) {
            log.error("{} pause failed", sysJob.getName());
        }
    }

    /**
     * 恢复工作
     *
     * @param scheduler 调度器
     * @param sysJob    系统工作
     */
    public static void resume(Scheduler scheduler, SysJob sysJob) {
        try {
            scheduler.resumeJob(getJobKey(sysJob.getId()));
        } catch (SchedulerException e) {
            log.error("{} resume failed", sysJob.getName());
        }
    }

    /**
     * 运行一次
     *
     * @param scheduler 调度器
     * @param sysJob    系统工作
     */
    public static void run(Scheduler scheduler, SysJob sysJob) {
        try {
            scheduler.triggerJob(getJobKey(sysJob.getId()));
        } catch (SchedulerException e) {
            log.error("{} run failed", sysJob.getName());
        }
    }

    public static TriggerKey getTriggerKey(Long jobId) {
        return new TriggerKey(SchedulerJobConfig.TRIGGER_PREFIX + jobId);
    }

    public static JobKey getJobKey(Long jobId) {
        return new JobKey(SchedulerJobConfig.JOB_PREFIX + jobId);
    }

    public static JobDetail getJobDetail(SysJob sysJob) {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put(SchedulerJobConfig.JOB_ENTITY, sysJob);
        return JobBuilder.newJob(SchedulerJob.class)
                .setJobData(jobDataMap)
                .withDescription(sysJob.getName())
                .withIdentity(getJobKey(sysJob.getId()))
                .build();
    }

    public static CronTrigger getCronTrigger(SysJob sysJob) {
        return TriggerBuilder
                .newTrigger()
                .withIdentity(getTriggerKey(sysJob.getId()))
                .withSchedule(CronScheduleBuilder.cronSchedule(sysJob.getCronExpression()).withMisfireHandlingInstructionDoNothing())
                .build();
    }
}