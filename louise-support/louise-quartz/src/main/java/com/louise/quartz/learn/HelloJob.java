package com.louise.quartz.learn;

import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * HelloJob
 *
 * @author louise
 * @date 2022/9/14
 */
@Slf4j
@DisallowConcurrentExecution
public class HelloJob implements Job {

    @Override
    public void execute(JobExecutionContext context) {
        Object triggerValue = context.getTrigger().getJobDataMap().get("triggerKey");
        Object jobValue = context.getJobDetail().getJobDataMap().get("jobKey");

        log.info("triggerKey : {}", triggerValue);
        log.info("jobKey : {}", jobValue);

        Object schedulerValue = null;
        try {
            schedulerValue = context.getScheduler().getContext().get("schedulerKey");
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

        log.info("schedulerKey : {}", schedulerValue);
        System.out.println("time : " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")));
    }
}