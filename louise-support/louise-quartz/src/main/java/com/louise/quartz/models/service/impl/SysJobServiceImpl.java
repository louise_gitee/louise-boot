package com.louise.quartz.models.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.louise.core.config.mybatisplus.common.AbstractServiceImpl;
import com.louise.quartz.models.entity.SysJob;
import com.louise.quartz.models.entity.vo.SysJobVo;
import com.louise.quartz.models.mapper.SysJobMapper;
import com.louise.quartz.models.service.SysJobService;
import org.springframework.stereotype.Service;

/**
 * SysJobServiceImpl
 *
 * @author louise
 * @date 2022/9/20
 */
@Service
public class SysJobServiceImpl extends AbstractServiceImpl<SysJobMapper, SysJob, SysJobVo> implements SysJobService {

    @Override
    public Wrapper<SysJob> getWrapper(SysJobVo vo) {
        return null;
    }
}