package com.louise.quartz.models.mapper;

import com.louise.quartz.models.entity.SysJobLog;
import org.apache.ibatis.annotations.Mapper;
import com.louise.core.config.mybatisplus.common.BasicMapper;

/**
 * sys_job_log Mapper
 *
 * @author 92017
 * 2024-04-06 01:02:07
 */
@Mapper
public interface SysJobLogMapper extends BasicMapper<SysJobLog> {

}




