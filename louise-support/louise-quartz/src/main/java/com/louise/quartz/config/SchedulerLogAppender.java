package com.louise.quartz.config;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.RollingFileAppender;


public class SchedulerLogAppender extends RollingFileAppender<ILoggingEvent> {

    @Override
    protected void append(ILoggingEvent event) {
        SchedulerJob.appendLog(encoder, event);
        super.append(event);
    }
}
