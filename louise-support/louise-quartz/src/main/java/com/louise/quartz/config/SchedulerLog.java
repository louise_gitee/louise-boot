package com.louise.quartz.config;

import org.apache.ibatis.logging.slf4j.Slf4jImpl;

/**
 * 用来保存数据库类型的log 暂时保留（因为JobLogAppender也能保存sql）
 *
 * @author louise
 * @date 2024/04/05
 */
public class SchedulerLog extends Slf4jImpl {

    public SchedulerLog(String clazz) {
        super(clazz);
    }

    @Override
    public void error(String s, Throwable e) {
        super.error(s, e);
    }

    @Override
    public void error(String s) {
        super.error(s);
    }

    @Override
    public void debug(String s) {
        super.debug(s);
    }

    @Override
    public void trace(String s) {
        super.trace(s);
    }

    @Override
    public void warn(String s) {
        super.warn(s);
    }

}
