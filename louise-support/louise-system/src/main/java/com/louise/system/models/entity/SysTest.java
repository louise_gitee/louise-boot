package com.louise.system.models.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * sys_test(测试表)
 *
 * @author Louise
 * 2024-04-02 14:44:15
 */
@Data
@TableName("sys_test")
public class SysTest implements Serializable {

    /**
     * id
     */
    @TableId
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 名称
     */
    private String name;

}
