package com.louise.system.models.mapper;

import com.louise.system.models.entity.SysTest;
import org.apache.ibatis.annotations.Mapper;
import com.louise.core.config.mybatisplus.common.BasicMapper;

/**
 * sys_test(测试表) Mapper
 *
 * @author Louise
 * 2024-04-02 14:44:15
 */
@Mapper
public interface SysTestMapper extends BasicMapper<SysTest> {

}




