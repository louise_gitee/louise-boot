package com.louise.system.models.entity.vo;

import com.louise.core.config.mybatisplus.common.BasicVo;

/**
 * sys_test(测试表) Vo
 *
 * @author Louise
 * 2024-04-02 14:44:15
 */
public class SysTestVo extends BasicVo {

}