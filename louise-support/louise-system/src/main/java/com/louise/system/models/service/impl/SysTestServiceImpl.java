package com.louise.system.models.service.impl;

import com.louise.system.models.entity.SysTest;
import com.louise.system.models.entity.vo.SysTestVo;
import com.louise.system.models.mapper.SysTestMapper;
import com.louise.system.models.service.SysTestService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.louise.core.config.mybatisplus.common.AbstractServiceImpl;

/**
 * sys_test(测试表) Service
 *
 * @author Louise
 * 2024-04-02 14:44:15
 */
@Service
public class SysTestServiceImpl extends AbstractServiceImpl<SysTestMapper, SysTest, SysTestVo>
        implements SysTestService {

    @Override
    public Wrapper<SysTest> getWrapper(SysTestVo vo) {
        return null;
    }
}




