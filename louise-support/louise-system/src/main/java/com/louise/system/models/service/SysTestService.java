package com.louise.system.models.service;

import com.louise.system.models.entity.SysTest;
import com.louise.system.models.entity.vo.SysTestVo;
import com.louise.core.config.mybatisplus.common.BasicService;

/**
 * sys_test(测试表) Service
 *
 * @author Louise
 * 2024-04-02 14:44:15
 */
public interface SysTestService extends BasicService<SysTest, SysTestVo> {

}
