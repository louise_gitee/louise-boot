package com.louise.system.models.controller;

import com.louise.core.config.mybatisplus.common.BasicController;
import com.louise.system.models.entity.SysTest;
import com.louise.system.models.entity.vo.SysTestVo;
import com.louise.system.models.service.SysTestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * sys_test(测试表) Controller
 *
 * @author Louise
 * 2024-04-02 14:44:15
 */
@Slf4j
@RestController
@RequestMapping("sysTest")
public class SysTestController extends BasicController<SysTestService, SysTest, SysTestVo> {

}




