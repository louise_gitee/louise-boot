package com.louise.core.utils;

import java.io.Serializable;
import java.lang.invoke.SerializedLambda;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


/**
 * 函数式接口
 *
 * @author Louise
 * @date 2022/08/22
 */
@FunctionalInterface
public interface MyFunction<T, R> extends Serializable {

    static void main(String[] args) {
        test(Test::setName, "123");
    }

    @SuppressWarnings("unchecked")
    static <T, R> void test(MyFunction<T, R> myFunction, R str) {
        System.out.println(myFunction.getImplClass());
        System.out.println(myFunction.getImplMethodName());
        Test test = new Test();
        myFunction.apply((T) test, str);
        System.out.println(test.getName());
    }

    class Test {
        String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    /**
     * 应用
     *
     * @param t t
     * @param r r
     */
    void apply(T t, R r);


    /**
     * 这个方法返回的SerializedLambda
     */
    default SerializedLambda getSerializedLambda() {
        try {
            Method write = this.getClass().getDeclaredMethod("writeReplace");
            write.setAccessible(true);
            return (SerializedLambda) write.invoke(this);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("no such method");
        } catch (IllegalAccessException e) {
            throw new RuntimeException("illegal access");
        } catch (InvocationTargetException e) {
            throw new RuntimeException("invocation target");
        }
    }

    /**
     * 查看实现类
     *
     * @return {@code String}  louise/enums/Status
     */
    default String getImplClass() {
        try {
            return getSerializedLambda().getImplClass();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 查看实现方法
     *
     * @return {@code String} setName
     */
    default String getImplMethodName() {
        return getSerializedLambda().getImplMethodName();
    }
}
