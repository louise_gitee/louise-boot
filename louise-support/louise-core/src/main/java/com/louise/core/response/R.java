package com.louise.core.response;

import lombok.Data;

@Data
public class R {
    Integer code;

    String msg;

    Object data;

    static final Integer SUCCESS_CODE = 200;
    static final String SUCCESS_MSG = "请求成功";

    static final Integer ERROR_CODE = 500;
    static final String ERROR_MSG = "服务器异常";

    public R(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public R(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static R success() {
        return new R(SUCCESS_CODE, SUCCESS_MSG);
    }

    public static R success(Object data) {
        return new R(SUCCESS_CODE, SUCCESS_MSG, data);
    }

    public static R error(String msg) {
        return new R(ERROR_CODE, msg);
    }

    public static R error(Integer code, String msg) {
        return new R(code, msg);
    }
}
