package ${controller.packageName};

import ${tableClass.fullClassName};
import ${vo.packageName}.${vo.fileName};
import ${serviceInterface.packageName}.${serviceInterface.fileName};
import com.louise.core.config.mybatisplus.common.BasicController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * ${tableClass.tableName}<#if tableClass.remark?has_content>(${tableClass.remark!})</#if> Controller
 *
 * @author ${author!}
 * ${.now?string('yyyy-MM-dd HH:mm:ss')}
 */
@RestController
@RequestMapping("${tableClass.shortClassName?uncap_first}")
public class ${controller.fileName} extends BasicController<${serviceInterface.fileName}, ${tableClass.shortClassName}, ${vo.fileName}> {

}




