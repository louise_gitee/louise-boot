package com.louise.core.config.mybatisplus;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.louise.core.config.mybatisplus.methods.SaveAllBath;
import com.louise.core.config.mybatisplus.methods.SaveAllBathIgnore;
import com.louise.core.config.mybatisplus.methods.SelectBatchByIdsForUpdate;
import com.louise.core.config.mybatisplus.methods.SelectByIdForUpdate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * MySqlInjector
 *
 * @author louise
 * @date 2022/9/4
 */
@Component
public class MySqlInjector extends DefaultSqlInjector {
    @Override
    public List<AbstractMethod> getMethodList(Class<?> mapperClass) {
        List<AbstractMethod> methodList = super.getMethodList(mapperClass);
        methodList.add(new SaveAllBath());
        methodList.add(new SaveAllBathIgnore());
        methodList.add(new SelectByIdForUpdate());
        methodList.add(new SelectBatchByIdsForUpdate());
        return methodList;
    }
}