package com.louise.core.utils;

import cn.hutool.core.util.ArrayUtil;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * 反射工具类
 *
 * @author louise
 * @date 2022/9/24
 */
public class ReflectUtil {

    /**
     * 获取实体类以及父类的所有字段
     */
    public static Field[] getAllField(Class<?> sourceClass) {
        Field[] arr = null;
        while (sourceClass != null) {
            arr = ArrayUtil.append(arr, sourceClass.getDeclaredFields());
            sourceClass = sourceClass.getSuperclass();
        }
        return arr;
    }

    /**
     * 获取泛型中的实际类型
     */
    @SuppressWarnings("unchecked")
    public static <T> Class<T> getActualType(Class<?> sourceClass, int index) {
        Type[] typeArguments = getActualTypeArguments(sourceClass);
        if (typeArguments == null || typeArguments.length < index - 1) {
            return null;
        }
        return (Class<T>) typeArguments[index - 1];
    }

    /**
     * 获取父类泛型数组
     *
     * @param sourceClass 源
     * @return {@code Type[]}
     */
    public static Type[] getActualTypeArguments(Class<?> sourceClass) {
        if (sourceClass == null) {
            return null;
        }
        Type genericSuperclass = sourceClass.getGenericSuperclass();
        if (genericSuperclass instanceof ParameterizedType) {
            return ((ParameterizedType) genericSuperclass).getActualTypeArguments();
        }
        return null;
    }
}