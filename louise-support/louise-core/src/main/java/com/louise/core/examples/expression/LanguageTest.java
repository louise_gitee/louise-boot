package com.louise.core.examples.expression;

import lombok.Data;

/**
 * LanguageTest
 *
 * @author louise
 * @date 2022/10/23
 */
@Data
public class LanguageTest {

    String username;

    String password;

    public LanguageTest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public void test(){
        System.out.println(username);
        System.out.println(password);
    }
}