package com.louise.core.config.mybatisplus.common;


import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.louise.core.config.validate.Save;
import com.louise.core.config.validate.Update;
import com.louise.core.response.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * BasicController
 * S 是 service
 * T 是 实体类
 * V 是 vo
 *
 * @author louise
 * @date 2022/9/8
 */
public class BasicController<S extends BasicService<T, V>, T, V extends BasicVo> {

    @Autowired
    protected S service;

    @PostMapping("list")
    public R list(@RequestBody V vo) {
        return R.success(service.list(service.getWrapper(vo)));
    }

    @PostMapping("page")
    public R page(@RequestBody V vo) {
        return R.success(service.page(service.getPage(vo), service.getWrapper(vo)));
    }

    @GetMapping("info/{id}")
    public R info(@PathVariable String id) {
        return R.success(service.getById(id));
    }

    @PostMapping("save")
    public R save(@Validated(Save.class) @RequestBody T t) {
        service.save(t);
        return R.success();
    }

    @PostMapping("update")
    public R update(@Validated(Update.class) @RequestBody T t) {
        service.updateById(t);
        return R.success();
    }

    @PostMapping("delete")
    public R delete(@RequestBody Long[] idList) {
        service.removeByIds(Arrays.asList(idList));
        return R.success();
    }

    @PostMapping("importExcel")
    @Transactional(rollbackFor = Exception.class)
    public R importExcel(MultipartFile file) throws IOException {
        List<T> list = EasyExcel.read(file.getInputStream(), service.getTClass(), null).sheet().doReadSync();
        service.saveBatch(list);
        return R.success();
    }

    @PostMapping("exportExcel")
    public void exportExcel(@RequestBody V vo, HttpServletResponse response) throws IOException {
        List<T> list = vo.isTemplate() ? Collections.emptyList() : service.list(service.getWrapper(vo));
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode("Excel导出", "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), service.getTClass()).excelType(ExcelTypeEnum.XLSX).sheet().doWrite(list);
    }
}