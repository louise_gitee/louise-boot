package com.louise.core.config.mybatisplus.common;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import lombok.Data;

import java.util.List;

/**
 * BasicVo
 *
 * @author louise
 * @date 2022/9/9
 */
@Data
public class BasicVo {
    Integer page;

    Integer size;

    List<OrderItem> orderList;

    boolean template = false;

    public Integer getPage() {
        return page == null ? 1 : page;
    }

    public Integer getSize() {
        return size == null ? 10 : size;
    }
}