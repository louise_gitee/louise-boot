package com.louise.core.config.mybatisplus.methods;

import com.baomidou.mybatisplus.core.metadata.TableInfo;
import org.apache.ibatis.executor.keygen.NoKeyGenerator;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

/**
 * mysql批量插入
 *
 * @author Louise
 * @date 2022/09/04
 */
public class SaveAllBath extends BaseAbstractMethod {

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        final String sql = MySqlMethod.SAVE_ALL_BATCH.getSql();
        final String fieldSql = prepareFieldSql(tableInfo);
        final String valueSql = prepareValuesSqlForBatch(tableInfo);
        final String sqlResult = String.format(sql, tableInfo.getTableName(), fieldSql, valueSql);
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sqlResult, modelClass);
        return this.addInsertMappedStatement(mapperClass, modelClass, MySqlMethod.SAVE_ALL_BATCH.getMethod(), sqlSource, new NoKeyGenerator(), null, null);
    }
}