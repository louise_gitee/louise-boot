package ${baseInfo.packageName};

import ${tableClass.fullClassName};
import ${vo.packageName}.${vo.fileName};
<#if baseService??&&baseService!="">
import ${baseService};
    <#list baseService?split(".") as simpleName>
        <#if !simpleName_has_next>
            <#assign serviceSimpleName>${simpleName}</#assign>
        </#if>
    </#list>
</#if>
import com.louise.core.config.mybatisplus.common.BasicService;

/**
 * ${tableClass.tableName}<#if tableClass.remark?has_content>(${tableClass.remark!})</#if> Service
 *
 * @author ${author!}
 * ${.now?string('yyyy-MM-dd HH:mm:ss')}
 */
public interface ${baseInfo.fileName} extends BasicService<${tableClass.shortClassName}, ${vo.fileName}> {

}
