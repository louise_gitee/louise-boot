package com.louise.core.config.mybatisplus.common;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

/**
 * BasicService
 *
 * T 为实体类
 * V 为vo
 *
 * @author louise
 * @date 2022/9/8
 */
public interface BasicService<T, V extends BasicVo> extends IService<T> {

    /**
     * 获取公共wrapper
     *
     * @param vo vo
     * @return {@code Wrapper<T>}
     */
    Wrapper<T> getWrapper(V vo);

    /**
     * mysql批量插入
     *
     * @param list 集合
     * @return int
     */
    int saveAllBath(@Param("list") Collection<T> list);

    /**
     * mysql批量插入 ignore
     *
     * @param list 集合
     * @return int
     */
    int saveAllBathIgnore(@Param("list") Collection<T> list);

    /**
     * 得到分页
     *
     * @param vo vo
     * @return {@code IPage<T>}
     */
    default IPage<T> getPage(V vo){
        Page<T> page = new Page<>(vo.getPage(), vo.getSize());
        if (vo.getOrderList() != null && vo.getOrderList().size() > 0){
            page.addOrder(vo.getOrderList());
        }
        return page;
    }

    @SuppressWarnings("unchecked")
    default Class<T> getTClass() {
        Class<?> aClass = this.getClass();
        Type genericSuperclass = aClass.getGenericSuperclass();
        if (genericSuperclass instanceof ParameterizedType){
            Type[] actualTypeArguments = ((ParameterizedType) genericSuperclass).getActualTypeArguments();
            return (Class<T>) actualTypeArguments[1];
        }
        return null;
    }
}
