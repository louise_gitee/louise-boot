package ${baseInfo.packageName};

import ${tableClass.fullClassName};
import ${vo.packageName}.${vo.fileName};
import ${mapperInterface.packageName}.${mapperInterface.fileName};
import ${serviceInterface.packageName}.${serviceInterface.fileName};
<#if baseService??&&baseService!="">
import ${baseService};
    <#list baseService?split(".") as simpleName>
        <#if !simpleName_has_next>
            <#assign serviceSimpleName>${simpleName}</#assign>
        </#if>
    </#list>
</#if>
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.louise.core.config.mybatisplus.common.AbstractServiceImpl;

/**
 * ${tableClass.tableName}<#if tableClass.remark?has_content>(${tableClass.remark!})</#if> Service
 *
 * @author ${author!}
 * ${.now?string('yyyy-MM-dd HH:mm:ss')}
 */
@Service
public class ${baseInfo.fileName} extends AbstractServiceImpl<${mapperInterface.fileName}, ${tableClass.shortClassName}, ${vo.fileName}>
        implements ${serviceInterface.fileName} {

    @Override
    public Wrapper<${tableClass.shortClassName}> getWrapper(${vo.fileName} vo) {
        return null;
    }
}




