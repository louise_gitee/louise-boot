package com.louise.core.examples.expression;

import org.junit.jupiter.api.Test;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.Date;

/**
 * test
 *
 * @author louise
 * @date 2022/9/6
 */
public class SpringExpressionLanguageTest {


    /**
     * 获取变量
     */
    @Test
    public void valueTest() {
        // 表达式解析器
        SpelExpressionParser spelExpressionParser = new SpelExpressionParser();
        // 解析出一个表达式
        Expression expression = spelExpressionParser.parseExpression("#user.password");
        // 开始准备表达式运行环境
        StandardEvaluationContext ctx = new StandardEvaluationContext();

        ctx.setVariable("user", new LanguageTest("123", "321"));

        String value = expression.getValue(ctx, String.class);

        System.out.println(value);
    }


    /**
     * 字符串拼接
     */
    @Test
    public void rootValueTest() {
        // 表达式解析器
        SpelExpressionParser spelExpressionParser = new SpelExpressionParser();
        // 解析出一个表达式
        Expression expression = spelExpressionParser.parseExpression("username + password");
        // 开始准备表达式运行环境
        StandardEvaluationContext ctx = new StandardEvaluationContext();

        ctx.setRootObject(new LanguageTest("123", "321"));

        String value = expression.getValue(ctx, String.class);

        System.out.println(value);
    }


    /**
     * 调用实体类方法
     */
    @Test
    public void entityMethodTest() {
        SpelExpressionParser spelExpressionParser = new SpelExpressionParser();

        Expression expression = spelExpressionParser.parseExpression("new com.louise.core.examples.expression.LanguageTest('123','456').test()");

        expression.getValue();
    }

    /**
     * 执行方法
     */
    @Test
    public void methodTest() {
        ExpressionParser parser = new SpelExpressionParser();
        //Expression expression = parser.parseExpression("setUsername('999')"); 可以有参数
        Expression expression = parser.parseExpression("getUsername()");

        StandardEvaluationContext ctx = new StandardEvaluationContext();

        ctx.setRootObject(new LanguageTest("123", "321"));

        String value = expression.getValue(ctx, String.class);

        System.out.println("value = " + value);
    }


    /**
     * 数字计算
     */
    @Test
    public void mathTest() {
        ExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression("1+2*3+3");
        Integer value = expression.getValue(Integer.class);
        System.out.println(value);
    }

    /**
     * 执行静态方法
     */
    @Test
    public void invokeStatic() {
        ExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression(
                "T(com.louise.core.examples.expression.SpringExpressionLanguageTest).staticMethod('123',321,new java.util.Date())");
        expression.getValue();
        staticMethod("321", 123, new Date());
    }

    public static void staticMethod(String str, Integer num, Date date) {
        System.out.printf("str : %s\n", str);
        System.out.printf("num : %s\n", num);
        System.out.printf("date : %s\n", date);
    }
}