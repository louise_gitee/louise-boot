package com.louise.core.examples.expression;

import org.springframework.context.ApplicationContext;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * SpringExpressionLanguageController
 *
 * @author louise
 * @date 2022/10/19
 */
@RestController
@RequestMapping("springExpressionLanguage")
public class SpringExpressionLanguageController {

    @Resource
    private ApplicationContext applicationContext;

    @GetMapping("springBeanTest")
    public void springBeanTest() {
        ExpressionParser parser = new SpelExpressionParser();

        StandardEvaluationContext context = new StandardEvaluationContext();

        context.setBeanResolver(new BeanFactoryResolver(applicationContext));

        Expression expression = parser.parseExpression("@springExpressionLanguageController.test1('t1',1)");

        expression.getValue(context);

        test1("123",321);
    }


    public void test1(String str, int integer) {
        System.out.printf("str : %s\n", str);
        System.out.printf("int : %s\n", integer);

    }
}