# 代码生成

1. 将 mybatis-plus4 文件夹复制到 `Scratches and Consoles` -> `Extensions` -> `MyBatisX` -> `templates`
2. idea 右边  `Database`  配置数据源，`tables` 中右键 `MybatisX-Generator`

# 表信息

| **名称**                  | **含义**                   |
| ------------------------- | -------------------------- |
| tableClass.fullClassName  | 类的全称(包括包名)         |
| tableClass.shortClassName | 类的简称                   |
| tableClass.tableName      | 表名                       |
| tableClass.pkFields       | 表的所有主键字段           |
| tableClass.allFields      | 表的所有字段               |
| tableClass.baseFields     | 排除主键和 blob 的所有字段 |
| tableClass.baseBlobFields | 排除主键的所有字段         |
| tableClass.remark         | 表注释                     |

# 字段信息

| 名称                | 含义                              |
| ------------------- | --------------------------------- |
| field.fieldName     | 字段名称                          |
| field.columnName    | 列名称                            |
| field.jdbcType      | jdbc 类型                         |
| field.columnLength  | 列段长度                          |
| field.columnScale   | 列的精度                          |
| field.columnIsArray | 字段类型是不是数组类型            |
| field.shortTypeName | java 类型短名称, 通常用于定义字段 |
| field.fullTypeName  | java 类型的长名称, 通常用于导入   |
| field.remark        | 字段注释                          |
| field.autoIncrement | 是否自增                          |
| field.nullable      | 是否允许为空                      |

# 配置信息

| 名称                    | 含义                   |
| ----------------------- | ---------------------- |
| baseInfo.shortClassName | 配置名称               |
| baseInfo.tableName      | 配置文件名称           |
| baseInfo.pkFields       | 配置名称               |
| baseInfo.allFields      | 后缀                   |
| baseInfo.baseFields     | 包名                   |
| baseInfo.baseBlobFields | 模板内容               |
| baseInfo.remark         | 相对模块的资源文件路径 |

# 其他

uncap_first  第一个单词首字母小写
cap_first    第一个单词首字母大写
capitalize   所有单词首字母大写

${tableClass.shortClassName?uncap_first}

${‘str’?substring(0,1)}             字符串截取
${“strabg”?replace(“ab”,”in”)}          字符串替换
${“string”?contains(“ing”)?string}  结果为true
${“string”?index_of(“in”)           结果为3  没有的话为 -1
