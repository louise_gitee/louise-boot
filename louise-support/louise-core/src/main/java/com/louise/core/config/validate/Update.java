package com.louise.core.config.validate;

import java.lang.annotation.*;

/**
 * Save
 *
 * @author louise
 * @date 2022/9/21
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Update {
}
