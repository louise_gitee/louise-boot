package com.louise.core.exception;

public enum ExceptionEnum {


    DEFAULT_ERROR(500,"服务器异常")
    ;

    private final Integer code;

    private final String message;

    ExceptionEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
