package com.louise.core.exception;

import com.louise.core.response.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {


    /**
     * 捕获 全局 报错
     */
    @ExceptionHandler(Throwable.class)
    public R errorHandler(Throwable throwable) {
        log.error("An error occurred", throwable);
        return R.error(throwable.getMessage());
    }

    /**
     * 捕获 {@link ServiceException} 报错
     */
    @ExceptionHandler(ServiceException.class)
    public R serviceExceptionHandler(ServiceException serviceException) {
        log.error("An error occurred", serviceException);
        return R.error(serviceException.getCode(), serviceException.getMessage());
    }

    /**
     * 捕获 Validated 报错
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public R methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException methodArgumentNotValidException) {
        log.error("An error occurred", methodArgumentNotValidException);
        return R.error(methodArgumentNotValidException
                .getFieldErrors().stream()
                .map(FieldError::getDefaultMessage)
                .collect(Collectors.joining(",")));
    }
}
