package com.louise.core.config.mybatisplus.common;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * BasicMapper
 *
 * @author louise
 * @date 2022/9/4
 */
public interface BasicMapper<T> extends BaseMapper<T> {

    /**
     * mysql批量插入
     *
     * @param list 集合
     * @return int
     */
    int saveAllBath(@Param("list") Collection<T> list);


    /**
     * mysql批量插入 ignore
     *
     * @param list 集合
     * @return int
     */
    int saveAllBathIgnore(@Param("list") Collection<T> list);


    /**
     * 根据id查询
     *
     * @param id id
     * @return {@code T}
     */
    T selectByIdForUpdate(Serializable id);


    /**
     * 根据id集合  行锁查询
     *
     * @param idList id集合
     * @return {@code List<T>}
     */
    List<T> selectBatchIdsForUpdate(@Param(Constants.COLLECTION) Collection<? extends Serializable> idList);
}
