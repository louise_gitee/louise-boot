package com.louise.core.config.mybatisplus.common;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * AbstractServiceImpl
 *
 * M 为mapper
 * T 为实体类
 * V 为vo
 *
 * @author louise
 * @date 2022/9/8
 */
public abstract class AbstractServiceImpl<M extends BasicMapper<T>, T, V extends BasicVo> extends ServiceImpl<M, T> implements BasicService<T, V> {

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int saveAllBath(Collection<T> list) {
        return baseMapper.saveAllBath(list);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int saveAllBathIgnore(Collection<T> list) {
        return baseMapper.saveAllBathIgnore(list);
    }
}