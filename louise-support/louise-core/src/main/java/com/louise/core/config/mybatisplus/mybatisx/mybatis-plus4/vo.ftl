package ${vo.packageName};

import com.louise.core.config.mybatisplus.common.BasicVo;

/**
 * ${tableClass.tableName}<#if tableClass.remark?has_content>(${tableClass.remark!})</#if> Vo
 *
 * @author ${author!}
 * ${.now?string('yyyy-MM-dd HH:mm:ss')}
 */
public class ${vo.fileName} extends BasicVo {

}