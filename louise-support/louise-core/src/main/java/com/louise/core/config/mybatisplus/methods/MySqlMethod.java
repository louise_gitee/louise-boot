package com.louise.core.config.mybatisplus.methods;

/**
 * MybatisPlus 支持 SQL 方法
 *
 * @author Louise
 * @date 2022/09/04
 */
public enum MySqlMethod {
    /**
     * 插入
     */
    SAVE_ALL_BATCH("saveAllBath", "批量插入", "<script>INSERT INTO %s %s VALUES %s</script>"),
    SAVE_ALL_BATCH_IGNORE("saveAllBathIgnore", "批量插入(ignore)", "<script>INSERT IGNORE INTO %s %s VALUES %s</script>"),
    SELECT_BY_ID_FOR_UPDATE("selectByIdForUpdate", "根据id 行锁查询", "SELECT %s FROM %s WHERE %s=#{%s} %s FOR UPDATE"),
    SELECT_BATCH_BY_IDS_FOR_UPDATE("selectBatchByIdsForUpdate", "根据id集合 行锁查询", "<script>SELECT %s FROM %s WHERE %s IN (%s) %s FOR UPDATE</script>");

    private final String method;
    private final String desc;
    private final String sql;

    MySqlMethod(String method, String desc, String sql) {
        this.method = method;
        this.desc = desc;
        this.sql = sql;
    }

    public String getMethod() {
        return method;
    }

    public String getDesc() {
        return desc;
    }

    public String getSql() {
        return sql;
    }

}
