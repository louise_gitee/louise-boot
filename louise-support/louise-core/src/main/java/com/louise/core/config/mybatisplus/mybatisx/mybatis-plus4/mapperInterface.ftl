package ${mapperInterface.packageName};

import ${tableClass.fullClassName};
<#if tableClass.pkFields??>
    <#list tableClass.pkFields as field><#assign pkName>${field.shortTypeName}</#assign></#list>
</#if>
import org.apache.ibatis.annotations.Mapper;
import com.louise.core.config.mybatisplus.common.BasicMapper;

/**
 * ${tableClass.tableName}<#if tableClass.remark?has_content>(${tableClass.remark!})</#if> Mapper
 *
 * @author ${author!}
 * ${.now?string('yyyy-MM-dd HH:mm:ss')}
 */
@Mapper
public interface ${mapperInterface.fileName} extends BasicMapper<${tableClass.shortClassName}> {

}




