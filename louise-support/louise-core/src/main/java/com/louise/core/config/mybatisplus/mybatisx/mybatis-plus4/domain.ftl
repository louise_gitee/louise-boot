package ${domain.packageName};

import java.io.Serializable;
<#list tableClass.importList as fieldType>${"\n"}import ${fieldType};</#list>
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * ${tableClass.tableName}<#if tableClass.remark?has_content>(${tableClass.remark!})</#if>
 *
 * @author ${author!}
 * ${.now?string('yyyy-MM-dd HH:mm:ss')}
 */
@TableName("${tableClass.tableName}")
@Data
public class ${tableClass.shortClassName} implements Serializable {

<#list tableClass.pkFields as field>
    /**
     * ${field.remark!}
     */
    @TableId
    private ${field.shortTypeName} ${field.fieldName};

</#list>
<#list tableClass.baseFields as field>
    /**
     * ${field.remark!}
     */
    private ${field.shortTypeName} ${field.fieldName};

</#list>
}
